var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var mongoose = require("mongoose")
var concat = require('concat-files');
var bodyParser = require('body-parser');
var fs = require('fs');
var async = require('async');
var Regex = require("regex");
var parse = require('csv-parse');
var geoip = require('geoip-lite');

mongoose.Promise=global.Promise


process.on('SIGINT', function () {
    console.log('db disconnect.');
    mongoose.disconnect()
    process.exit(2);
  });

mongoose.connect("mongodb://localhost:27017/nervaAppdb",{useNewUrlParser: true});
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var app = express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')

var input_1_flag=0
var input_2_flag=0
var input_3_flag=0
var input_4_flag=0
var value=0
var BTC_price=0
var trade_price=0
var NH=0
var HW_pre=2.7
var ER_pre=0.1
var R=0
var countryJson={}
//var M=0
//var F0=0.001/1024

var nameSchema = new mongoose.Schema({
	NH:String,
	R:String,
	xnvbtc:String,
	btcvalue:String,
	HW:String,
	ER:String,
	realvalue:String,
	marketvalue:String,
	ratio:String,
	date:{type: Date, default: Date.now}
	
})
var data1 = mongoose.model("nerva",nameSchema)

app.get('/getdayincome', function (req, res) {
	var hps=parseFloat(req.query.hps)
	if(hps==0){
		hps=200
	}
	var income = (hps/NH)*R*60*24
	res.send(income.toFixed(3).toString())
		
})

app.get('/getdayincomed', function (req, res) {
	var hps_t=req.query.hps
	var hps=parseFloat(hps_t)
	if(hps==NaN){
		hps=200
	}
	var income = (hps/NH)*R*60*24*trade_price*BTC_price
	res.send(income.toFixed(3).toString())
		
})

app.get('/getrecords', function (req, res) {
	var query = data1.find();

	// selecting the 'name' and 'age' fields
	query.select('marketvalue ratio date');


	query.limit(120);

	// sort by age
	query.sort({ date: -1 });

	// execute the query at a later time
	query.exec(function (err, data) {
	  if (err) return handleError(err);
	  res.send(data)
	})
		
})

app.get('/getcountries', function (req, res) {

	  res.send(countryJson)
	
})
	

app.get('/getratio', function (req, res) {
	var hpw=req.query.hpw
	var er=req.query.er

	var HW=parseFloat(hpw)
    var ER=parseFloat(er)
    
    if(hpw==0){
		HW=HW_pre
	}
	
	if(er==0){
		ER=ER_pre
	}
    
    var value=(NH/(HW*R))*(1/(60*1000))*ER
	
	var market_value=trade_price*BTC_price
	var ratio=(market_value/value) *100
	
	res.send(ratio.toFixed(2).toString())
})

app.get('/getreal', function (req, res) {
	var hpw=req.query.hpw
	var er=req.query.er

	var HW=parseFloat(hpw)
    var ER=parseFloat(er)
     if(hpw==0){
		HW=HW_pre
	}
	if(er==0){
		ER=ER_pre
	}
    
    value=(NH/(HW*R))*(1/(60*1000))*ER

	res.send(value.toFixed(3).toString())
})
app.get('/getmarket', function (req, res) {
	var market_value=trade_price*BTC_price
	var market_str =market_value.toFixed(4).toString()
	res.send(market_str)
})
app.get('/getnh', function (req, res) {
	
	res.send((NH/1000000).toFixed(2).toString())
})

app.get('/getr', function (req, res) {
	
	res.send(R.toFixed(2).toString())
})

app.get('/getxnv', function (req, res) {
	
	res.send(trade_price.toString())
})

app.get('/getbtc', function (req, res) {
	
	res.send(BTC_price.toString())
})

//Page initial load function
app.get('/', function (req, res) {
  var HW=HW_pre
  var ER=ER_pre
  var value=(NH/(HW*R))*(1/(60*1000))*ER
  
  var market_value=trade_price*BTC_price

  var real_str =value.toFixed(3).toString()
  var market_str =market_value.toFixed(4).toString()
  var ratio=market_value/value *100
  var ratio_str=ratio.toFixed(2).toString()
  var NH_str=(NH/1000000).toFixed(2).toString()
  var R_str=R.toFixed(2).toString()
  var xnvbtc_str=trade_price.toString()
  var btcusd_str=BTC_price.toString()
  //var feevalue=(calc_fee()*trade_price*BTC_price).toFixed(4).toString()
  //var fee=(calc_fee()).toFixed(6).toString()
  res.render('index', {xnv_btc:xnvbtc_str,btc_usd:btcusd_str,nh:NH_str,r:R_str,real: real_str, market: market_str,ratio:ratio_str,ratio_val:ratio,hpw:null,er:null,error: null});
  
})

//server start
var server = app.listen(8080, function () {
   var host = server.address().address
   var port = server.address().port
  
   console.log("Web app listening at http://%s:%s", host, port)
})

//update data from api periodically every 1 min
function intervalFunc() {
  request({url:'https://blockchain.info/ticker',json:true}, function (error, response, body) 	{
	  if(response.statusCode==200){
		  var json=body
		  BTC_price=json.USD.last
		  console.log('BTC_price Updated!');
		  
		  input_1_flag=1
	  }

  })
  
  request({url:'https://tradeogre.com/api/v1/ticker/BTC-XNV',json:true}, function (error, response, body) 	{
	if(response.statusCode==200){
		var json=body
		trade_price=json.price
		console.log('trade_price Updated!');
		input_2_flag=1
	}
  })
  
  request({url:'https://api.getnerva.org/getlastblockheader.php',json:true}, 
  function (error, response, body) 	{
	  if(response.statusCode==200){
		  var json=body.result.block_header
		  NH=json.difficulty/60
		  R=json.reward/Math.pow(10,12) 
		  console.log('NH,R Updated!');
		  input_3_flag=1
	  }
  })
  /*
  request({url:'https://api.getnerva.org/getinfo.php',json:true}, 
  function (error, response, body) 	{
	  if(response.statusCode==200){
		  M=body.result.block_size_median
		  var input_4_flag=1
 
	  }
  })*/
  
}

function checkDataComplete(){
	
	if(input_1_flag ==1 && input_2_flag ==1 && input_3_flag==1){
		console.log('checkDataComplete Func')
		input_1_flag=0
		input_2_flag=0
		input_3_flag=0
		//calculate for database
		var HW=HW_pre
		var ER=ER_pre
		var value=(NH/(HW*R))*(1/(60*1000))*ER

		var market_value=trade_price*BTC_price

		var real_str =value.toFixed(3).toString()
		var market_str =market_value.toFixed(4).toString()
		var ratio=market_value/value *100
		var ratio_str=ratio.toFixed(2).toString()
		var NH_str=(NH/1000000).toFixed(2).toString()
		var R_str=R.toFixed(2).toString()
		var xnvbtc_str=trade_price.toString()
		var btcusd_str=BTC_price.toString()
		

		//save to database 

		var myData=new data1({NH:NH,R:R,xnvbtc:trade_price,btcvalue:BTC_price,HW:HW,ER:ER,realvalue:value,marketvalue:market_str,ratio:ratio})
		
		myData.save(function(err){
		  if(err){
			   console.log(err);
			   return;
		  }

		});
		
		//console.log(myData)
      
	}
}

    
function decodeIP(){
	var total_count=0
	var country_list=[]
	var ip_all=[];
	console.log("IP decode start.");

	
	var bodyParser = require('body-parser');
	var fs = require('fs');
	var async = require('async');
	var Regex = require("regex");
	var parse = require('csv-parse');
	//const iplocation = require("iplocation").default;
	var geoip = require('geoip-lite');

	 concat([
    'ip',
    'ip2',
    'ip3',
	], 'ip_final', function(err) {
		if (err) return
		console.log('done concat ip files');

	

		var stream = fs.createWriteStream("cleaned_ip");
		stream.once('open', function(fd) {
			
			var lineReader = require('readline').createInterface({
			  input: require('fs').createReadStream("ip_final")
			});

			lineReader.on('line', function (line) {
				line = line.replace(/ +/g, ' ');
				stream.write(line+'\n');
  
			}).on('close', function(line) {
				stream.end()
				fs.createReadStream('cleaned_ip')
					.pipe(parse({delimiter: ' '}))
					.on('data', function(csvrow) {
						
						if(csvrow[0]=="white"){	
							var ip=csvrow[2].split(':')
							ip_all.push(ip[0])
							total_count++
							//console.log(ip[0]);
						}		
					}).on('end',function() {
							console.log(ip_all);
							ip_all.filter((v, i, a) => a.indexOf(v) == i)
							for (var item in ip_all){
								var geo = geoip.lookup(ip_all[item]);
								if(geo!=null){
									country_list.push(geo.country)		 
								} 
							}
							 
							country_list.sort();
							
							var current = null;
							var cnt = 0;
							for (var i = 0; i < country_list.length; i++) {
								if (country_list[i] != current) {
									if (cnt > 0) {
										//console.log(current +':' + cnt);
										countryJson[current.toLowerCase()]=cnt
									}
									current = country_list[i];
									cnt = 1;
								} else {
									cnt++;
								}
							}
							if (cnt > 0) {
								//console.log(current + ":"+cnt);
								countryJson[current.toLowerCase()]=cnt
							}
							console.log(countryJson);
							console.log("IP decode finished.");
							console.log("total node count:"+total_count);

					});

				});
			});
		})
		
}

decodeIP()
intervalFunc()
setInterval(intervalFunc, 60000);	//long period incase of blacklisting of ip from third party api providers

setInterval(checkDataComplete, 1000);

setInterval(decodeIP, 60000);




