# NervaWebApp

## Dashboard

This web app can be used to get the approximate cost to make one Nerva coin and the current market price

Also includes: 

World map of active nodes(experimental) 

2 hours of price graph 

2 hours of cost to price ratio graph

income calculator from hash value.

## Requirements:

npm

sudo apt install nodejs

mongoDB

sudo apt install -y mongodb


## To get dependencies type:

npm install

## To run:

node index.js

## To access the app go to :

localhost:8080

![snap](/img.png)

